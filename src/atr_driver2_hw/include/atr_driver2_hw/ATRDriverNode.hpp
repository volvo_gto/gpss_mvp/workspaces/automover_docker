/*! \file
 *
 * \author Emmanuel Dean
 *
 * \version 0.1
 * \date 13.06.2022
 *
 *
 * #### License
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 * #### Acknowledgment
 *  Adapted from https://github.com/ros2-realtime-demo/pendulum.git
 */

/// \file
/// \brief

#ifndef ATR_DRIVER__ATR_DRIVER_NODE_HPP_
#define ATR_DRIVER__ATR_DRIVER_NODE_HPP_

/*! \file ATRController.hpp
 *  \brief This file provides a ROS 2 interface to implement the inverted atr driver.
 *
 *  Provides the following functionalities:
 *      - ATR state pubisher
 *      - JointState publisher (to visualize the ATR in rviz using a robotState publisher)
 *      - Commanded wheel velocities subscriber
 *      - lifecycle states
 */

#include <memory>
#include <string>

#include <rclcpp/rclcpp.hpp>
#include <rclcpp_lifecycle/lifecycle_node.hpp>
#include <lifecycle_msgs/msg/transition_event.hpp>
#include <rclcpp/strategies/message_pool_memory_strategy.hpp>
#include <rclcpp/strategies/allocator_memory_strategy.hpp>
#include <gpss_control_interfaces/msg/speed_ref.hpp>
#include <gpss_control_interfaces/msg/speed_act.hpp>


#include "atr_utils/math_defs.h"

// #include "atr_driver2_hw/am_driver_node.h"

#include "atr_automower/AutomowerSafe.h"

namespace atr
{
namespace atr_driver
{
/**
 *  @class This class implements a node containing a simulated atr or the drivers for a real one.
 */

class ATRDriverNode : public rclcpp_lifecycle::LifecycleNode
{
public:
  using SubSpeedRef = std::shared_ptr<rclcpp::Subscription<gpss_control_interfaces::msg::SpeedRef>>;
  using LifeCycleCallbackReturn = rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn;
  using PubSpeedAct = std::shared_ptr<rclcpp_lifecycle::LifecyclePublisher<gpss_control_interfaces::msg::SpeedAct>>;

private:
  const std::string atr_id_;                                      ///< atr's ID
  const std::string atr_frame_id_;                  ///< topic to receive the commanded wheel velocities
  std::chrono::microseconds state_publish_period_;        ///< period in seconds for the driver thread
  std::chrono::milliseconds deadline_duration_;           ///< deadline time to detect missed messages
  SubSpeedRef command_sub_;                               ///< subscriber to receive the commanded wheel velocities
  PubSpeedAct state_pub_;                                 ///< Publisher for state
  rclcpp::TimerBase::SharedPtr state_timer_;              ///< main thread
  int msg_counter,callback_counter;                       ///< counters to count msg recieved and callback    

  uint32_t num_missed_deadlines_sub_;        ///< missed msg counter cmd wheel velocities
  Husqvarna::AutomowerSafePtr am_;  ///< automower hardware access class
  std::vector<double> linearization_param_;  ///< linearization parameters k0 and k1

  std::mutex cmd_mutex_;
  gpss_control_interfaces::msg::SpeedRef speed_ref_;


public:
  /// \brief Default constructor, needed for node composition
  /// \param[in] options Node options for rclcpp internals
  explicit ATRDriverNode(const rclcpp::NodeOptions& options);

  /// \brief Parameter file constructor
  /// \param[in] node_name Name of this node
  /// \param[in] options Node options for rclcpp internals
  explicit ATRDriverNode(const std::string& node_name,
                                           const rclcpp::NodeOptions& options = rclcpp::NodeOptions());

private:

  /// \brief Create command subscription
  void create_command_subscription();


  /**
   * @brief Create a automower connection object
   *
   */
  void create_automower_connection();


  /// \brief Create timer callback
  void create_state_timer_callback();


  /// \brief Transition callback for state configuring
  /// \param[in] lifecycle node state

  LifeCycleCallbackReturn on_configure(const rclcpp_lifecycle::State&) override;

  /// \brief Transition callback for state activating
  /// \param[in] lifecycle node state
  LifeCycleCallbackReturn on_activate(const rclcpp_lifecycle::State&) override;

  /// \brief Transition callback for state deactivating
  /// \param[in] lifecycle node state
  LifeCycleCallbackReturn on_deactivate(const rclcpp_lifecycle::State&) override;

  /// \brief Transition callback for state cleaningup
  /// \param[in] lifecycle node state
  LifeCycleCallbackReturn on_cleanup(const rclcpp_lifecycle::State&) override;

  /// \brief Transition callback for state shutting down
  /// \param[in] lifecycle node state
  LifeCycleCallbackReturn on_shutdown(const rclcpp_lifecycle::State& state) override;
};
}  // namespace atr_driver
}  // namespace atr

#endif  // ATR_DRIVER__ATR_DRIVER_NODE_HPP_
