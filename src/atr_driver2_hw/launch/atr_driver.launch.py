#  \file
#
# \author Emmanuel Dean
#
# \version 0.1
# \date 13.06.2022
#
# \copyright Copyright 2022 Chalmers
#
### License
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

import os

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.conditions import IfCondition, UnlessCondition
import launch.substitutions
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare

import yaml


def generate_launch_description():

    # Create the launch description and populate
    ld = LaunchDescription()

    # atr_id_param = DeclareLaunchArgument(name="atr_id", default_value="1", description="Defines the atr ID")
    # ld.add_action(atr_id_param)

    # Create the launch configuration variables
    autostart_param = DeclareLaunchArgument(
        name="autostart", default_value="True", description="Automatically start lifecycle nodes"
    )
    priority_param = DeclareLaunchArgument(name="priority", default_value="0", description="Set process priority")
    ld.add_action(autostart_param)
    ld.add_action(priority_param)

    # CPU affinity defines the CPUs used to run a process (and its threads)
    # This parameter defines the CPU ids, after converting it into binary format.
    # For example,  cpu-affinity=0 --> uses all the available CPUs, cpu-affinity 4 = 0100 --> CPU 2, cpu-affinity 3 = 0011 --> CPUs 0,1
    cpu_affinity_param = DeclareLaunchArgument(
        name="cpu-affinity", default_value="0", description="Set process CPU affinity"
    )
    ld.add_action(cpu_affinity_param)

    with_lock_memory_param = DeclareLaunchArgument(
        name="lock-memory", default_value="False", description="Lock the process memory"
    )
    ld.add_action(with_lock_memory_param)

    lock_memory_size_param = DeclareLaunchArgument(
        name="lock-memory-size", default_value="0", description="Set lock memory size in MB"
    )
    ld.add_action(lock_memory_size_param)

    config_child_threads_param = DeclareLaunchArgument(
        name="config-child-threads",
        default_value="False",
        description="Configure process child threads (typically DDS threads)",
    )
    ld.add_action(config_child_threads_param)
    
    atr_id = os.getenv('ATR_ID')
    if atr_id is None:
        atr_id = '1'

    print("Launching ATR: " + atr_id)

    # Set parameter file path
    param_file_path = os.path.join("db","config", "atr_demo_hw.param.yaml")
    param_file = launch.substitutions.LaunchConfiguration("params", default=[param_file_path])
    # Node definitions
    # Runs:
    #  The atr driver:  Computes the state using integrator and the control input "joint_command"
    # (atr2_msgs::JointState) and publishes the "atr_joint_states" (atr2_msgs::JointState)
    #  The atr controller: Computes the control command "joint_command" based on the current state subscriber
    # "atr_joint_states"
    # With Debugger
    # https://answers.ros.org/question/385292/debugging-ros-2-cpp-node-with-vs-code-starting-the-node-with-a-launch-file/
    atr_demo_hw_runner = Node(
        package="atr_driver2_hw",
        executable="atr_driver",
        # prefix=["gdbserver localhost:3000"],
        output="screen",
        parameters=[param_file],
        arguments=[
            "--autostart",
            LaunchConfiguration("autostart"),
            "--priority",
            LaunchConfiguration("priority"),
            "--cpu-affinity",
            LaunchConfiguration("cpu-affinity"),
            "--lock-memory",
            LaunchConfiguration("lock-memory"),
            "--lock-memory-size",
            LaunchConfiguration("lock-memory-size"),
            "--config-child-threads",
            LaunchConfiguration("config-child-threads"),
            "--id",
            atr_id,
        ],
        emulate_tty=True,
    )
    ld.add_action(atr_demo_hw_runner)

    return ld
