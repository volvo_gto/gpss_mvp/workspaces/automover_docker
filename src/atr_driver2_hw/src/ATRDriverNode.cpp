/**
 * \author Emmanuel Dean
 *
 * \version 0.1
 * \date 13.06.2022
 *
 * \copyright Copyright 2022 Chalmers
 *
 * #### Licence
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 * #### Acknowledgment
 *  Modified from https://github.com/ros2-realtime-demo/pendulum.git
 */

#include <string>
#include <memory>

#include "rclcpp/strategies/message_pool_memory_strategy.hpp"
#include "rclcpp/strategies/allocator_memory_strategy.hpp"

#include "atr_driver2_hw/ATRDriverNode.hpp"
// #include "atr_utils/AuxTools.h"



namespace atr
{
namespace atr_driver
{
ATRDriverNode::ATRDriverNode(const rclcpp::NodeOptions& options) : ATRDriverNode("atr_driver", options)
{
}

//clang-format off
ATRDriverNode::ATRDriverNode(const std::string& node_name, const rclcpp::NodeOptions& options)
  : LifecycleNode(node_name, options)
  , atr_id_(declare_parameter<std::string>("atr_id", "1"))
  , atr_frame_id_(declare_parameter<std::string>("frame_id", "map"))
  , state_publish_period_(
        std::chrono::microseconds{ declare_parameter<std::uint16_t>("state_publish_period_us", 10000U) })
  , deadline_duration_{ std::chrono::milliseconds{ declare_parameter<std::uint16_t>("deadline_duration_ms", 0U) } }
  , num_missed_deadlines_sub_{ 0U }
  , am_(std::make_shared<Husqvarna::AutomowerSafe>())
  , linearization_param_(
        declare_parameter<std::vector<double>>("driver.linearization_param", { -0.08, 1.2 }))
// clang-format on
{



  state_pub_ = this->LifecycleNode::create_publisher<gpss_control_interfaces::msg::SpeedAct>("/"+atr_id_ + "/speed", 10);

  create_command_subscription();

  create_automower_connection();

  create_state_timer_callback();
}



void ATRDriverNode::create_command_subscription()
{
  // Pre-allocates message in a pool
  using rclcpp::memory_strategies::allocator_memory_strategy::AllocatorMemoryStrategy;
  using rclcpp::strategies::message_pool_memory_strategy::MessagePoolMemoryStrategy;
  auto command_msg_strategy = std::make_shared<MessagePoolMemoryStrategy<gpss_control_interfaces::msg::SpeedRef, 1>>();

  rclcpp::SubscriptionOptions command_subscription_options;
  command_subscription_options.event_callbacks.deadline_callback = [this](rclcpp::QOSDeadlineRequestedInfo&) -> void {
    num_missed_deadlines_sub_++;
  };

  auto on_command_received = [this](gpss_control_interfaces::msg::SpeedRef::SharedPtr msg) {
    msg_counter = callback_counter;
    std::lock_guard<std::mutex> guard(cmd_mutex_);
    speed_ref_ = *msg;
  };
  command_sub_ = this->create_subscription<gpss_control_interfaces::msg::SpeedRef>(
      "/"+atr_id_ + "/cmd", rclcpp::QoS(10).deadline(deadline_duration_), on_command_received,
      command_subscription_options, command_msg_strategy);
}


void ATRDriverNode::create_state_timer_callback()
{
 
  auto state_timer_callback = [this]() {
    am_->getSensorStatus();
    
    // TODO: use the atate_publish_period to calculate this frequency
    // am_->updateOdometry(100.0);
    //RCLCPP_INFO_STREAM(get_logger(), "am pose: " << am_->xdist << ", " << am_->ydist << ", " << am_->delta_yaw);

    // TODO: transform the above distance from ATR frame to wcf

    am_->getWheelData();

    //RCLCPP_INFO(get_logger(), "msg_counter: %d", msg_counter);
    callback_counter = callback_counter + 1;
    //RCLCPP_INFO(get_logger(), "msg_counter: %d", msg_counter);
    //RCLCPP_INFO(get_logger(), "callback_counter: %d", callback_counter);

    {
      std::lock_guard<std::mutex> guard(cmd_mutex_);
      am_->wanted_lv = speed_ref_.ref_speed_left_wheel;
      am_->wanted_rv = speed_ref_.ref_speed_right_wheel;
    }

    if (callback_counter > 1000)
    {
      callback_counter = 0;
    }
  
    if ((callback_counter - msg_counter) > 50)
    {
      am_->wanted_lv = 0.0;
      am_->wanted_rv = 0.0;
    }

    auto msg = std::make_unique<gpss_control_interfaces::msg::SpeedAct>();
    msg->status = am_-> status;
    msg->act_speed_left_wheel = am_-> current_lv;
    msg->act_speed_right_wheel = am_-> current_rv;
    msg->act_power_left_wheel = am_-> power_l;
    msg->act_power_right_wheel = am_-> power_r;

    state_pub_->publish(std::move(msg));

    // Send wheel commands to the HW

    if (am_-> sensor_status & 0x0004)
    {
      am_->sendWheelPower(0.0, 0.0);
      RCLCPP_INFO(get_logger(), "Collision - Node - stop wheels");
    }
    else
    {


      if ((am_->wanted_rv < 0.025) && (am_->wanted_lv < 0.025) && (am_->wanted_rv > -0.025) &&
          (am_->wanted_lv > -0.025))
      {
        am_->wanted_lv = 0.0;
        am_->wanted_rv = 0.0;
      }

      am_->regulateVelocity();
      //am_->regulateVelocity(linearization_param_.at(0),linearization_param_.at(1));
      


    }
  };
  state_timer_ = this->create_wall_timer(state_publish_period_, state_timer_callback);

  // cancel immediately to prevent triggering it in this state
  state_timer_->cancel();
}

void ATRDriverNode::create_automower_connection()
{
  am_->setup();
  am_->initAutomowerBoard(linearization_param_.at(0), linearization_param_.at(1));
}






rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_configure(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Configuring");
  // reset internal state of the driver for a clean start
  
  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_activate(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Activating");
  state_pub_->on_activate();
  state_timer_->reset();

  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_deactivate(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Deactivating");
  state_timer_->cancel();
  state_pub_->on_deactivate();
  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_cleanup(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Cleaning up");
  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_shutdown(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Shutting down");
  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}
}  // namespace atr_driver
}  // namespace atr

#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point,
// allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(atr::atr_driver::ATRDriverNode)
