/*
 * Copyright (c) 2014 - Husqvarna AB, part of HusqvarnaGroup
 * Author: Stefan Grufman
 *  	   Kent Askenmalm
 *
 */

#ifndef AUTOMOWER_SAFE_H
#define AUTOMOWER_SAFE_H

#include <rclcpp/rclcpp.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <sys/select.h>


#ifdef __cplusplus
extern "C" {
#endif

// To get rid of loads of compiler warnings...
#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "atr_hcp/hcp/hcp_types.h"
#include "atr_hcp/hcp/hcp_runtime.h"
#include "atr_hcp/hcp/hcp_string.h"
#include "atr_hcp/hcp/hcp_library.h"

#include "atr_hcp/hcp/amg3.h"


#pragma GCC diagnostic pop

typedef struct tCodec
{
  char* path;
  hcp_szStr name;
  hcp_tCodecLibrary* lib;
} tCodec;

typedef struct
{
  hcp_tVector header;
  hcp_tCodec fixed[HCP_MAXSIZE_CODECS];
} tCodecSet;

#ifdef __cplusplus
}
#endif

namespace Husqvarna
{
#define AM_STATE_UNDEFINED 0x0
#define AM_STATE_IDLE 0x1
#define AM_STATE_INIT 0x2
#define AM_STATE_MANUAL 0x3
#define AM_STATE_RANDOM 0x4
#define AM_STATE_PARK 0x5

class AutomowerSafe
{
public:
  // const ros::NodeHandle& nodeh, decision_making::RosEventQueue* eq);
  AutomowerSafe();
  ~AutomowerSafe();

  bool setup();
  bool update();  // rclcpp::Duration dt);

  // decision_making::RosEventQueue* eventQueue;

  bool m_regulatingActive;

  void pauseMower();
  void startMower();
  void setParkMode();
  void setAutoMode();
  void cutDiscHandling();
  void cutDiscOff();
  void stopWheels();
  void loopDetectionHandling();
  void cuttingHeightHandling();

  void newControlMainState(int aNewState);

  int GetUpdateRate();
  double wanted_lv, wanted_rv;
  double k0_g, k1_g;
  double AUTMOWER_WHEEL_BASE_WIDTH;
  void regulateVelocity();
  bool initAutomowerBoard(double k0, double k1);
  void updateOdometry(double updateFreq);
  // bool getEncoderData();
  bool getWheelData();
  bool getSensorStatus();


  double xpos, ypos, yaw;
  double xdist, ydist, delta_yaw;
  void sendWheelPower(double power_left, double power_right);
  int16_t power_l, power_r;
  double current_lv, current_rv;
  std::string status;
  int sensor_status;
  int operational_mode;
  int mower_internal_state;
  int control_state;

private:

  std::string resultToString(hcp_tResult result);

  bool sendMessage(const char* msg, int len, hcp_tResult& result);
  void setPower();

  bool getSensorData();
  bool getPitchAndRoll();
  bool getGPSData();
  bool getStateData();

  bool getLoopData();
  bool getBatteryData();

  bool isTimeOut();  // rclcpp::Duration elapsedTime, double frequency);

  // bool executeTifCommand(am_driver_safe::TifCmd::Request& req,
  // am_driver_safe::TifCmd::Response& res); Removed

  // double regulatePid(double current_vel, double wanted_vel);
  bool doSerialComTest();
  void handleCollisionInjections();  // rclcpp::Duration dt);

  // ROS data
  // ros::NodeHandle nh;

  // ros::ServiceServer tifCommandService;
  // ros::Subscriber velocity_sub;
  // ros::Subscriber power_sub;
  // ros::Subscriber cmd_sub;
  // ros::Subscriber imu_sub;
  // ros::Subscriber imu_euler_sub;

  // ros::Publisher pose_pub;
  // ros::Publisher odom_pub;
  // ros::Publisher euler_pub;

  // ros::Publisher loop_pub;
  // ros::Publisher sensorStatus_pub;
  // ros::Publisher batStatus_pub;

  // ros::Publisher encoder_pub;
  // ros::Publisher current_pub;
  // ros::Publisher wheelPower_pub;
  // ros::Publisher motorFeedbackDiffDrive_pub;

  // ros::Publisher navSatFix_pub;
  // tf::TransformBroadcaster br;

  double updateRate;

  double encoderSensorFreq;
  double wheelSensorFreq;
  // double regulatorFreq;
  double setPowerFreq;
  double stateCheckFreq;
  double loopSensorFreq;
  double batteryCheckFreq;
  double GPSCheckFreq;
  double pitchRollFreq;
  double sensorStatusCheckFreq;

  std::string loadJsonModel(std::string fileName);

  // rclcpp::Duration timeSinceWheelSensor;
  // rclcpp::Duration timeSinceEncoderSensor;
  // rclcpp::Duration timeSinceRegulator;
  // rclcpp::Duration timeSinceState;
  // rclcpp::Duration timeSinceLoop;
  // rclcpp::Duration timeSincePitchRoll;
  // rclcpp::Duration timeSincebattery;
  // rclcpp::Duration timeSinceGPS;
  // rclcpp::Duration timeSinceStatus;
  // rclcpp::Duration timeSinceCollision;

  // Control data
  double lin_vel;
  double ang_vel;

  double wanted_power_left, wanted_power_right;

  double last_yaw;

  bool automowerInterfaceInited;
  int leftPulses;
  int rightPulses;
  int lastLeftPulses;
  int lastRightPulses;
  int leftTicks;
  int rightTicks;

  // Parameters
  std::string pSerialPort;
  std::string robot_name;
  bool serialComTest;
  bool serialLog;

  // Mower parameters (treated as const, that is why capital letters...sorry)
  double WHEEL_DIAMETER;
  int WHEEL_PULSES_PER_TURN;
  double WHEEL_METER_PER_TICK;

  // Serial port handle
  int serialFd;

  // Serial port state
  int serialPortState;

  // Automower status

  unsigned short requestedState;

  int publishTf;
  int velocityRegulator;
  bool regulateBySpeed;

  bool newSound;
  char soundCmd[100];

  int collisionState;

  // Cutting disc
  bool cuttingDiscOn;
  bool lastCuttingDiscOn;
  unsigned char cuttingHeight;
  unsigned char lastCuttingHeight;

  uint8_t actionResponse;

  bool requestedLoopOn;

  // For HCP Library
  hcp_tHost hcpHost;
  hcp_tState* hcpState;
  tCodecSet hcpCodecs;
  std::string jsonFile;
  hcp_Size_t codecId;
  hcp_Int modelId;

  double lastRateCheckTime;
  int lastComtestWheelMotorPower;
  double startTime;

  bool startWithoutLoop;
  bool publishEuler;

  bool printCharge;

  // UserStop
  bool userStop;

  // Accelerometer
  bool m_PitchAndRollFromAccelerometer;
  double m_pitch;
  double m_roll;

  // GPS
  bool m_publishGPS;

  std::vector<double> c_;

  // tf::Transform transform;
};

using AutomowerSafePtr = std::shared_ptr<AutomowerSafe>;

}  // namespace Husqvarna

#endif
