#!/bin/bash

# Starting the zenoh bridge
/zenoh-b-pi -e tcp/10.57.197.71:7449 -e tcp/10.57.197.72:7449 -e tcp/10.57.197.73:7449 -d $ATR_ID -m client --no-multicast-scouting &
  
# Start the second process
ros2 launch atr_driver2_hw atr_driver.launch.py &
  
# Wait for any process to exit
wait -n
  
# Exit with status of process that exited first
exit $?

